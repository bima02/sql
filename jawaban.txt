1.
	CREATE DATABASE myshop;



2. 

create table users(
id int(8) primary key auto_increment,
name varchar(255),
email varchar(255),
password varchar(255));

create table categories(
id int(8) primary key auto_increment,
name varchar(255));

create table items(
id int(8) primary key auto_increment,
name varchar(255),
description varchar(255),
price int,
stock int,
category_id int(8),
FOREIGN KEY (category_id) REFERENCES categories(id));



3.

INSERT INTO users(name,email,password)
VALUES ('John Doe','john@doe.com','john123'),('Jane Doe','jane@doe.com','jenita123');


INSERT INTO categories(name)
VALUES ('gadget'),('cloth'),('men'),('women'),('branded');


INSERT INTO items(name,description,price,stock,category_id)
VALUES ('Sumsang b50','hape keren dari merek sumsang',4000000,100,1),('Uniklooh','baju keren dari brand ternama',500000,50,2),('IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);



4.

a. Mengambil data users
select name, email from users;

b. Mengambil data items
select * from items where price > 1000000;

select * from items where name like '%sang%';

c. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name
FROM items
INNER JOIN categories ON items.category_id = categories.id;


5. 
UPDATE items
SET price=2500000
WHERE name='sumsang b50';
